# React Pagination Component Challenge

## Overview

Welcome to the React Pagination Component Challenge! In this coding challenge, you will create a functional pagination component that interacts with a mock API to fetch and display data. This challenge is designed to test your skills in API integration, state management, and implementing pagination logic within a React application.

## Challenge Description

Your task is to implement a pagination component in React that fetches data from a simulated API, displays a fixed number of items per page, and provides user controls to navigate between pages.

### Requirements

1. **Implement the Pagination Component UI**:
   - Create a user-friendly interface that displays items and pagination controls (e.g., next, previous, specific page numbers).

2. **Fetch Data from a Mock API**:
   - Simulate an API using a delay to mimic network latency. You can use static data for the items.
   - Utilize either the native `fetch` API or a library like `axios` for making HTTP requests.

3. **Display a Fixed Number of Items Per Page**:
   - The component should allow displaying a configurable number of items per page (e.g., 10 items per page).
   - Ensure the pagination controls update dynamically based on the total number of items fetched from the API.

4. **State Management**:
   - Use React state to manage the current page and the total number of items.
   - Ensure that the UI updates correctly when users navigate between pages or when new data is fetched.

### Technologies

- **React**: The primary framework for building the component.
- **Axios or Fetch API**: For making HTTP requests to the mock API.
- **CSS/SCSS** (optional): For styling the component. Feel free to use any CSS framework like Bootstrap or TailwindCSS if preferred.

### Bonus (Optional)

- Implement advanced pagination features such as:
  - Jump to the first and last pages.
  - Display ellipses (...) when there are too many pages to show simultaneously.
  - Responsive design for better mobile support.

## Submission Guidelines

- Provide the source code for the pagination component.
- Include a `README.md` file with:
  - Instructions on how to set up and run your project.
  - A brief explanation of the design choices and technologies used.
- Ensure your code is well-documented and follows best practices for readability and maintainability.

## Evaluation Criteria

- **Functionality**: The component should meet all the specified requirements.
- **Code Quality**: Code should be clean, well-organized, and properly documented.
- **UI Design**: The pagination component should be intuitive and user-friendly.
- **Creativity and Use of Technology**: Innovative use of technology and design will be considered.

Get ready to showcase your React and front-end development skills. Good luck!